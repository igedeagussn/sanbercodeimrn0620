//Soal Nomor 1//

var nama = ""
var peran = ""

if (nama == "" ) {
    console.log("Nama harus diisi!")
} else if (peran == "") {
    console.log("Halo "+ nama + ", Pilih peranmu untuk memulai game!")
} else if (peran == "Penyihir") {
    console.log("Selamat datang di Dunia Werewolf, "+ nama) 
    console.log ("Halo Penyihir " + nama + " , kamu dapat melihat siapa yang menjadi werewolf!" )
} else if (peran == "Guard") {
    console.log("Selamat datang di Dunia Werewolf, "+ nama)
    console.log ("Halo Guard " + nama + " , kamu akan membantu melindungi temanmu dari serangan werewolf." )
} else if (peran=="Werewolf") {
    console.log("Selamat datang di Dunia Werewolf, "+ nama)
    console.log ("Halo Werewolf " + nama + " , Kamu akan memakan mangsa setiap malam!" )
}

console.log("====================================================")
//Soal Nomor 2//

var hari = 21; 
var bulan = 12; 
var tahun = 1945;
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945'; 

var tanggal; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

switch(bulan) {
  case 1:   { var bulan = "Januari"; break; }
  case 2:   { var bulan = "Februari"; break; }
  case 3:   { var bulan = "Maret"; break; }
  case 4:   { var bulan = "April"; break; }
  case 5:   { var bulan = "Mei"; break; }
  case 6:   { var bulan = "Juni"; break; }
  case 7:   { var bulan = "Juli"; break; }
  case 8:   { var bulan = "Agustus"; break; }
  case 9:   { var bulan = "September"; break; }
  case 10:   { var bulan = "Oktober"; break; }
  case 11:   { var bulan = "November"; break; }
  case 12:   { var bulan = "Desember"; break; }

  default:  { console.log('Masukkan variabel bulan antara  Bulan ke-1 hingga Bulan ke-12'); }}

  console.log(hari + " " + bulan + " " + tahun);