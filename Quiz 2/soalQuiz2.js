console.log("soal nomor 1")
console.log(" ")

/*Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
"points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
    constructor(){
        this.subject = "Math"
        this.email = "susisusanti@gmail.com"
    }
    average=(points)=> {
        let lengpoints=points.length
        let sumpoints=0
        if(lengpoints==undefined){
            avpoints=points
        } else{
        for (var i=0; i<lengpoints; i++){
            sumpoints=sumpoints+points[i]
        }
        var avpoints=sumpoints/lengpoints}
        return(`${this.subject} ${this.email}, ${avpoints}`)
      }
}

myScore= new Score()
console.log(myScore.average([100,80,60]))
console.log(myScore.average(100))

console.log("==================")
console.log("soal nomor 2")
console.log(" ")

/*Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 

  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/

const data = [
  ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

function viewScores(data, subject) {
 
    var dataleng=data.length
  let quiz1Num=1
  let quiz2Num=2
  let quiz3Num=3
    var nilai=[]
  switch(subject) {
    case 'quiz-1':   { j=1; break; }
    case 'quiz-2':   { j=2;  break;}
    case 'quiz-3':   { j=3; break; }
    default:  { console.log('Masukkan Rute'); }}
  
    for (i=1; i<=j.length;i++){
        nilai[i] = {
          email : data[i][0],
          subject : subject,
          points : data[i][1]}  
  
} 
console.log(nilai[1]+nilai[2])

}
 
// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")



console.log("==================")
console.log("soal nomor 3")
console.log(" ")

/*Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/

function recapScores(data){
    for (i=1;i<data.length;i++){
       let avg = (data[i][1]+data[i][2]+data[i][3])/3
        if (avg>90){
            predikat="honour"
        } else if (avg>80){
            predikat="graduate"
        } else {
            predikat="participant"
        }
        console.log(`${i}. Email: ${data[i][0]}`)
        console.log(`Rata-rata: ${avg.toFixed(1)}`)
        console.log(`Predikat: ${predikat}` )
        console.log("")
    }
}

recapScores(data)

