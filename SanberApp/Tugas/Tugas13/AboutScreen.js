import React, {Component} from 'react';
import { Platform, StatusBar, StyleSheet,FlatList, Text, TouchableOpacity, View, Image, TouchableOpacityBase } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';


export default class App extends Component {
  render(){
  return (
    <View style={styles.container}>
      
     <View style={styles.navBar}>
         <Text style={styles.Headline}>Account</Text>
     </View>
     <View style={styles.body}>
     <View style={styles.content}>
     <Image source={require('./Image/logo_big.png')} style={{width: 194, height:126}}/>
     <View style={styles.desc}>
     <Text>code.io is an Indonesia based startup company that contains social network that focuses on professional networking and career development for programmer or developers.</Text>
     </View>
    
         </View>
         <View style={styles.divider}>
        </View>
        
       <View style={styles.detail}>
       <Text style={styles.Title}>Our Developer</Text>
        <View style={styles.imagedev}>
        <Image source={require('./Image/agus.png')} style={{width: 80, height:80}}/>
        </View>
        <View style={styles.name}>
        <Text style={styles.nameTitle}>I Gede Agus Surya Negara</Text>
        <Text style={styles.address}>South Jakarta, Indonesia</Text>
        </View>
        <View style={styles.socmed}>
        <Image source={require('./Image/instagram.png')} style={{width: 16, height:16}}/>
        <Text style={styles.socmedTitle}>@gdagussn</Text>   
        </View>
        <View style={styles.socmed}>
        <Image source={require('./Image/dribbble.png')} style={{width: 16, height:16}}/>
        <Text style={styles.socmedTitle}>I Gede Agus Surya Negara</Text>   
        </View>
        <View style={styles.socmed}>
        <Image source={require('./Image/gitlab.png')} style={{width: 16, height:16}}/>
        <Text style={styles.socmedTitle}>@igedeagussn</Text>   
        </View>
    
       </View>
     </View>
     

       <View style={styles.tabBar}>
        <TouchableOpacity style={styles.tabItem}>
        <Icon name='home' size={25} color="#80807F"/>
        <Text style={styles.tabTitle}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
        <Icon name='public' size={25} color="#80807F"/>
        <Text style={styles.tabTitle}>Explore</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
        <Icon name='search' size={25} color="#80807F"/>
        <Text style={styles.tabTitle}>Search</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
        <Icon name='person' size={25} color="#CCAD02"/>
        <Text style={styles.tabTitleSelected}>About Us</Text>
        </TouchableOpacity>
        
      </View>
     
       
    </View>
  
  );
  }
}

const styles = StyleSheet.create({
    container: {     
        flex: 1,
        backgroundColor: '#FFFFFE'
      },
  
      navBar: {
        height: 56, 
        backgroundColor: '#FFFFFE', 
        elevation: 3,
        alignItems: 'center',
        paddingVertical: 13
    },

        Headline:{
            fontWeight:'bold',
            fontSize:20,
            color:"#2D334A"
        },
        content:{
         
            marginHorizontal:16,
            marginTop:20,
            alignItems: 'center'
        },
        desc:{
           marginTop: 16, 
        },
        divider: {
            marginVertical: 20,
            marginHorizontal:16,
            height:1,
            backgroundColor:'#E5E5E5'
          },
          Title:{
              fontSize:24,
              fontWeight:'bold',
          },
          detail:{
              alignItems:'center',
          },
          imagedev: {
              marginTop:12
          },
          name: {
              marginTop:8,
              flexDirection:'column',
              alignItems: 'center'
          },
          nameTitle: {
              fontSize: 12,
              fontWeight: 'bold'
          },
          address:{
              fontSize:10,
              fontWeight: 'normal',
              color:'#2D334A'
          },
          socmed:{
              marginTop:8,
              alignItems: 'center',
              flexDirection: 'row',
              

          },
          socmedTitle:{
              marginHorizontal:8,
              color:"#575C6E",
              fontSize:10
          },
          body:{
              flex:1
          },
          
          tabBar: {
            
            backgroundColor: 'white',
            height: 60,
            borderTopWidth:0.5,
            borderColor:'#E5E5E5',
            flexDirection: 'row',
            justifyContent: 'space-around'
          },
          tabItem: {
            alignItems: 'center',
            justifyContent: 'center'
          },
          tabTitle: {
            fontSize: 11,
            color: '#9699A4',
            paddingTop: 4
          },
          tabTitleSelected: {
            fontSize: 11,
            color: '#CCAD02',
            paddingTop: 4
          }
    
});

