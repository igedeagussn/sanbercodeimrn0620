import React, {Component} from 'react';
import { Platform, StatusBar, StyleSheet,FlatList, Text, TouchableOpacity, View, Image, TouchableOpacityBase, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import data from './skillData.json'
import Skills from './skill'

export default class App extends Component {
  render(){
  return (
    <View style={styles.container}>
      
     <View style={styles.navBar}>
        <Image source={require('./Image/codeio.png')} style={{width: 56, height:37}}/>
        <Icon style={styles.navItem} name='notifications-none' size={24}/>
     </View>
     <View style={styles.body}>
     <View style={styles.HeadlineArea}>
        <View style={styles.headline}>
             <Text style={styles.textHeadline}>Bima Sakti</Text>
            <Text style={styles.textSubHeadline}>Freelance Programmer</Text>
            <Text style={styles.textDetailHeadline}>Jakarta, Indonesia</Text>
        </View>
        <Image source={require('./Image/profpic.png')} style={{width: 80, height:80}}/>

     </View>

     <View style={styles.divider}>
     </View>

     <View style={styles.Section}>
         <View style={styles.SectionTitle}>
            <Text style={styles.sectionTitle}>Top Skillset</Text>
            <Text style={styles.textDetailHeadline}>Bima's Top Skillset</Text>
         </View>
        
        <Text style={styles.buttonText}>View All</Text>   
     </View>
     <View>
        <FlatList 
            horizontal pagingEnabled={true}
            showsHorizontalScrollIndicator={false}
            data={data.items}
            renderItem={(skill)=><Skills skill={skill.item}/>}
            keyExtractor={(item)=>item.id}>         
        </FlatList>
        </View>
        <View style={styles.divider}>
     </View>
        <View style={styles.Section}>
         <View style={styles.SectionTitle}>
            <Text style={styles.sectionTitle}>Experiences</Text>
            <Text style={styles.textDetailHeadline}>Bima’s  latest projects or work experiences.</Text>
         </View>
        
        <Text style={styles.buttonText}>View All</Text>   
     </View>  

     <ScrollView horizontal={true} style={styles.Scrollview} showsHorizontalScrollIndicator={false}>
            <View style={styles.CardExperiences}>
               <View style={styles.ExperienceTitleArea}>
                     <Text style={styles.ExperienceTitle}>Website Feature Improvement</Text> 
                     <Image source={require('./Image/samsung.png')} style={{width: 32, height:24}}/>
                     <Text style={styles.textDetailHeadline}>Mar 2017 - Mar 2019</Text>

               </View>
                 

            </View>
            <View style={styles.CardExperiences}>
            <View style={styles.ExperienceTitleArea}>
                     <Text style={styles.ExperienceTitle}>iOs and Android Development</Text> 
                     <Image source={require('./Image/gojek.png')} style={{width: 32, height:24}}/>
                     <Text style={styles.textDetailHeadline}>May 2017 - Mar 2018</Text>

               </View>    

            </View>
        </ScrollView>    

    
         


     
     
     
     </View>
     

       <View style={styles.tabBar}>
        <TouchableOpacity style={styles.tabItem}>
        <Icon name='home' size={25} color="#CCAD02"/>
        <Text style={styles.tabTitleSelected}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
        <Icon name='public' size={25} color="#80807F"/>
        <Text style={styles.tabTitle}>Explore</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
        <Icon name='search' size={25} color="#80807F"/>
        <Text style={styles.tabTitle}>Search</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.tabItem}>
        <Icon name='person' size={25} color="#80807F"/>
        <Text style={styles.tabTitle}>About Us</Text>
        </TouchableOpacity>
        
      </View>
     
       
    </View>
  
  );
  }
}

const styles = StyleSheet.create({
    container: {     
        flex: 1,
        backgroundColor: '#FFFFFE',
      },
  
      navBar: {
        height: 56, 
        backgroundColor: '#FFFFFE', 
        elevation: 3,
        paddingHorizontal: 16,
        flexDirection:"row",
        justifyContent:'space-between',
        alignItems:'center'
    },

    ExperienceTitle:{
        fontWeight:'bold',
        marginTop: 12,
        
    },

    sectionBorder:{
        marginBottom: 24
    },
        HeadlineArea:{
            marginTop:24,
            flexDirection:'row',
            justifyContent:'space-between',
            paddingHorizontal:16
        },
        headline:{
            
            
        },

        ExperienceTitleArea:{
           marginHorizontal:8
           
        },
        Scrollview:{
            paddingHorizontal:16,
            flexDirection:'row',
            

        },
        Card:{
            marginLeft:0,
            marginHorizontal:16,
            marginTop:12,
            width:117,
            height:148,
            elevation:3,
            borderRadius:5,
            borderWidth:1,
            borderColor:'#ECEBED',
            backgroundColor:'#FFFFFE'
        },

        CardExperiences:{
            marginLeft:0,
            marginHorizontal:16,
            marginTop:12,
            width:180,
            height:100,
            elevation:3,
            borderRadius:5,
            borderWidth:1,
            borderColor:'#ECEBED',
            backgroundColor:'#FFFFFE'
        },

        buttonText:{
            marginTop: 20,
            fontSize:12,
            fontWeight:'bold',
            color:'#2D334A'
        },

        Section:{
            
            paddingHorizontal:16,
            flexDirection: 'row',
            justifyContent:'space-between'
        },

        sectionTitle:{
            
        },
        textHeadline:{
            fontWeight:'bold',
            fontSize:28,
            color:'#2D334A'
        },

        sectionTitle:{
            fontSize:16,
            fontWeight:'bold',
            color: '#2D334A',
            marginVertical:4
        },

        textSubHeadline:{
            color:'#575C6E',
            fontSize:16,
        },

        horizontal:{
            flexDirection:'row'
        },
        textDetailHeadline:{
            color:'#575C6E',
            fontSize:12,
            margin:2
        },

        desc:{
           marginTop: 16, 
        },
        divider: {
            marginVertical: 20,
            marginHorizontal:16,
            height:1,
            backgroundColor:'#E5E5E5'
          },
          Title:{
              fontSize:24,
              fontWeight:'bold',
          },
          detail:{
              alignItems:'center',
          },
          imagedev: {
              marginTop:12
          },
          name: {
              marginTop:8,
              flexDirection:'column',
              alignItems: 'center'
          },
          nameTitle: {
              fontSize: 12,
              fontWeight: 'bold'
          },
          address:{
              fontSize:10,
              fontWeight: 'normal',
              color:'#2D334A'
          },
          socmed:{
              marginTop:8,
              alignItems: 'center',
              flexDirection: 'row',
              

          },
          socmedTitle:{
              marginHorizontal:8,
              color:"#575C6E",
              fontSize:10
          },
          body:{
              flex:1
          },
          
          tabBar: {
            
            backgroundColor: 'white',
            height: 60,
            borderTopWidth:0.5,
            borderColor:'#E5E5E5',
            flexDirection: 'row',
            justifyContent: 'space-around'
          },
          tabItem: {
            alignItems: 'center',
            justifyContent: 'center'
          },
          tabTitle: {
            fontSize: 11,
            color: '#9699A4',
            paddingTop: 4
          },
          tabTitleSelected: {
            fontSize: 11,
            color: '#CCAD02',
            paddingTop: 4
          },

          Cardsection:{
              flex:1
          }
    
});

