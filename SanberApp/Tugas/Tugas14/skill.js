import React, {Component} from 'react';
import { Playform, StyleSheet, Text, Flatlist,TouchableOpacity, View, Image, TouchableOpacityBase,ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'; 

export default class Skills extends Component {
 
 render(){
    let skill = this.props.skill;
    return(
    <View style={styles.container}>
        <ScrollView horizontal={true} style={styles.Scrollview} showsHorizontalScrollIndicator={false}>
            <View style={styles.Card}>
                    <Text style={styles.cardTitle}>{skill.categoryName}</Text>
                    <Text style={styles.cardSubTitle}>{skill.category}</Text>
                    <View style={styles.iconDetail}>
                    <Icon name={skill.iconName} size={24} color='#CCAD02'/>
                    <Text style={styles.skillTitle}>{skill.skillName}</Text>
                    </View>
                    <Text style={styles.percentage}>{skill.percentageProgress}</Text>

            </View>
        </ScrollView>    
            
        </View>
    )
 }
}

  
const styles = StyleSheet.create({
    container: {
        flex:1,
    },
    Card:{
        justifyContent:'space-around',
        paddingHorizontal:8,
        marginLeft:0,
        marginTop:12,
        width:117,
        height:148,
        elevation:3,
        borderRadius:5,
        borderWidth:1,
        borderColor:'#ECEBED',
        backgroundColor:'#FFFFFE'
    },
    percentage:{
        marginTop:0,
        fontSize:24,
        fontWeight:'bold',
        color:'#CCAD02'
    },
    skillTitle:{
        marginTop:4,
        marginHorizontal:4,
        fontSize:12,
        color:'#818592',
        marginBottom:8

    },
    iconDetail:{
        flexDirection:'row',
        alignItems:'center'
    },
    descContainer:{
        flexDirection : 'column',
        paddingTop: 12,
    },

    Scrollview:{
        marginLeft:16,
        flexDirection:'row',
        

    },

    cardTitle:{
        fontSize:12,
        fontWeight:'bold',
        marginTop:8,
        marginBottom:0,
        color:'#272343'

    },

    cardSubTitle:{
        fontSize:12,
        marginTop:4,
        color: '#3c3c3c',
        marginBottom:16
    },
    videoTitle: {
        fontSize : 16,
        color: '#3c3c3c'
    
    },
    videoDetails:{
        paddingHorizontal: 15,
        flex:1
    },
    videoStats: {
        fontSize: 15,
        paddingTop: 3,
        color: '#888888'
    }
    
});