import React, {Component} from 'react';
import { Platform, StatusBar, Button, StyleSheet,FlatList, Text, TouchableOpacity, View, Image, TouchableOpacityBase, TouchableNativeFeedback } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const ScreenContainer = ({ children }) => (
    <View style={styles.container}>{children}</View>
  );

export const AddScreen = ({navigation}) => (
    <ScreenContainer>
      <Text>Tambah Proyek</Text>
      </ScreenContainer>
);

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    paddingTop:64,
    flex: 1,
    backgroundColor: '#FFFFFE'
  }
}
)