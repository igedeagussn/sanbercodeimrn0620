import * as React from 'react';
import { Button, View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';
import { render } from 'react-dom';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {LoginScreen} from './LoginScreen'
import {SkillScreen} from './SkillScreen'
import{AboutScreen} from './AboutScreen'
import {AddScreen} from './AddScreen'
import { ProjectScreen } from './ProjectScreen'


const LoginStack =createStackNavigator();
const Tabs =createBottomTabNavigator();
const SkillStack =createStackNavigator();
const AddStack =createStackNavigator();
const AboutStack =createStackNavigator();
const ProjectStack =createStackNavigator();

const LoginStackScreen = () => (
  <LoginStack.Navigator>
    <LoginStack.Screen name="LoginScreen" component={LoginScreen} />
  </LoginStack.Navigator>
)

const SkillStackScreen = () => (
  <SkillStack.Navigator>
    <SkillStack.Screen name="SkillScreen" component={SkillScreen} />
  </SkillStack.Navigator>
)

const AboutStackScreen = () => (
  <AboutStack.Navigator>
    <AboutStack.Screen name="AboutScreen" component={AboutScreen} />
  </AboutStack.Navigator>
)

const AddStackScreen = () => (
  <AddStack.Navigator>
    <AddStack.Screen name="AddScreen" component={AddScreen} />
  </AddStack.Navigator>
)

const ProjectStackScreen = () => (
  <ProjectStack.Navigator>
    <ProjectStack.Screen name="ProjectScreen" component={ProjectScreen} />
  </ProjectStack.Navigator>
)

const TabsScreen = () => (
  <Tabs.Navigator>
        <Tabs.Screen name="SkillScreen" component={SkillStackScreen}/>
        <Tabs.Screen name="ProjectScreen" component={ProjectStackScreen}/>
        <Tabs.Screen name="AddScreen" component={AddStackScreen}/>
      </Tabs.Navigator>
)

const Drawer = createDrawerNavigator();


// Screen Login



// Screen Profile
function ProfilesScreen({ route, navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Profile Screen</Text>
      <Button
        color="black"
        title="Go to Home"
        onPress={() => navigation.navigate('Home')}
      />
      <Button title="Go back" onPress={() => navigation.goBack()} />
      <Button
        color="green"
        title="Go back to first screen in stack"
        onPress={() => navigation.popToTop()}
      />
    </View>
  );
}

// Stack berguna untuk routing aplikasi
const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
      
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="Skill Screen" component={SkillScreen} />
        <Stack.Screen name="Profiles" component={ProfilesScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default()=>(   
  <NavigationContainer>
    <Drawer.Navigator initialRouteName="LoginScreen">
    <Drawer.Screen name="LoginScreen" component={LoginScreen} />
    <Drawer.Screen name="AboutScreen" component={AboutScreen} />
    <Drawer.Screen name="TabsScreen" component={TabsScreen} />


  </Drawer.Navigator>

   {/* <AuthStack.Navigator>
      <AuthStack.Screen name='SignIn' component={SignIn}/>
      <AuthStack.Screen name='CreateAccount' component={CreateAccount}/>
   </AuthStack.Navigator> */}
  </NavigationContainer>
    
 
);
