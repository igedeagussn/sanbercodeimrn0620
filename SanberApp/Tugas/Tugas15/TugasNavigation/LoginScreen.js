import React, {Component} from 'react';
import { Platform, StatusBar, Button, StyleSheet,FlatList, Text, TouchableOpacity, View, Image, TouchableOpacityBase, TouchableNativeFeedback } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const ScreenContainer = ({ children }) => (
    <View style={styles.container}>{children}</View>
  );

export const LoginScreen = ({navigation}) => (
    <ScreenContainer>
    <View style={styles.container}>
       <Image source={require('./Image/logo.png')} style={{width: 132, height:80}}/>
        <View style={styles.content}>
        <Text style={styles.Headline}>Welcome, coder.</Text>
        <View style={styles.loginArea}>
            <View style={styles.box}>
                <View style={styles.DetailLogin}>   
                    <Text style={styles.TextLogin}>Username or email</Text>
                </View>
            </View>
            
            <View style={styles.box}>
                <View style={styles.DetailLogin}>
                    <Text style={styles.TextLogin}>Password</Text>
                    <TouchableOpacity>
                    <Icon style={styles.navItem} name='visibility' size={24} color="#9699A4"/>
                    </TouchableOpacity>
                </View>
            
            <View style={styles.BottomLoginBox}>
                <Text style={styles.ButtonText}> Forgot Password? </Text>
                
                <Button
                     color="orange"
                    title="Login"
                   
                     />
               
            </View>
       
         

            <View style={styles.otherLoginArea}>
                 <View style={styles.divider}>
                 </View>
                 <Text>or</Text>
                 <View style={styles.divider}></View>
            </View>

        
            <View style={styles.buttonOtherLogin}>
                <View style={styles.Logo}>
                    <Image source={require('./Image/google.png')} style={{width: 24, height:24}}/>
                </View>
               
                <View style={styles.verticalDivider}>

                </View>
                <View style={styles.CTATextGoogle}>
                    <Text style={styles.ButtonGoogle}>Login with Google</Text>
                </View>
               
               

            </View>
          
            <View style={styles.SignUp}>
                 <Text>Don't have an account?</Text>
                 <Text style={styles.SignUpButton}>Sign Up</Text>
            </View>
            </View>
        </View>
       
        </View>
      
    </View>
    </ScreenContainer>
  )


const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    paddingTop:64,
    flex: 1,
    backgroundColor: '#FFFFFE'
  },
  content: {
      alignItems: 'center',
      paddingHorizontal: 32,
      paddingVertical: 20,
      flex:1
  },
  Headline: {
      fontFamily: 'Roboto',
      fontSize: 20,
      color: '#2D334A',
      fontWeight:'bold'
  },
  box: {
      width:296,
      height:48,
      margin:16,
      backgroundColor: '#FFFFFE',
      borderWidth:1,
      borderColor: "#524F68",
      borderRadius: 10

  },
  loginArea: {
      paddingTop: 24,
      paddingHorizontal:32
  },
  DetailLogin : {
      paddingHorizontal: 16,
      paddingVertical: 12,
      flexDirection: 'row',
      justifyContent: 'space-between'
  },
  TextLogin : {
    color: "#9699A4"
},

ButtonText: {
    fontFamily: 'Roboto',
    fontSize: 12,
    fontWeight: 'bold'
},

Button: {
    alignItems: 'center',
    marginTop:4,
    width:112,
    height:48,
    backgroundColor: "#FFD803",
    borderRadius: 10,
},

BottomLoginBox: {
    paddingTop: 12,
    flexDirection: 'row',
    justifyContent: 'space-between'
},
  ButtonAction:{
      fontFamily: 'Roboto',
      fontSize: 16,
      color: 272343,
      fontWeight: 'bold',
      paddingTop: 12
  },
  otherLoginArea: {
      marginTop: 48,

      flexDirection: 'row',
      justifyContent: 'space-around'
  },
  
  divider: {
    marginTop: 12,
    width: 128,
    height:1,
    backgroundColor:'#E5E5E5'
  },

  buttonOtherLogin:{
      marginTop: 16,
    
      color: '#FFFFFE',
      borderWidth: 1,
      borderRadius:10,
      borderColor: '#272343',
      width: 296,
      height:51,
      flexDirection: 'row',
  },

  verticalDivider: {
    height:49,
    width:1,
    backgroundColor: '#272343'
     
  },
  Logo:{
      marginHorizontal:16,
      marginVertical:12
  },

  CTATextGoogle:{
      marginVertical:12,
      marginHorizontal: 47,
      
  },
  ButtonGoogle:{
      fontWeight: 'bold'
  },
 
  SignUp: {
      marginTop: 83,
      alignItems: 'center',
      flexDirection: 'row',
      justifyContent: 'center'
    
  },

  SignUpButton:{
      color: '#E5C203',
      marginHorizontal: 8,
      fontWeight: 'bold'
  }

});

